{ sources ? import ./nix/sources.nix }:

let
  php_version = "7.4";

  pkgs = import sources.nixpkgs { overlays = []; config = {}; };
  phps = (import sources.nix-phps).packages.${builtins.currentSystem};

  php = phps."php${builtins.replaceStrings ["."] [""] php_version}";
  niv = (import sources.niv {}).niv;
in

pkgs.mkShell {
  buildInputs = [
    (php.withExtensions ({ all, ... }: with all; [
        iconv
        json
        mbstring
    ]))

    php.packages.composer

    niv
  ];
}
