<?php

namespace LuthfiSolahudin\Sqlgen\Console\Providers;

use Illuminate\Support\ServiceProvider;
use LuthfiSolahudin\Sqlgen\Parser\GenericParser;
use LuthfiSolahudin\Sqlgen\Parser\ParserInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ParserInterface::class, GenericParser::class);
    }
}
