<?php

namespace LuthfiSolahudin\Sqlgen\Console\Commands;

use LaravelZero\Framework\Commands\Command;
use LuthfiSolahudin\Sqlgen\Parser\ParserInterface;
use Symfony\Component\Console\Input\InputOption;

class GenerateCommand extends Command
{
    protected ParserInterface $parser;

    public function __construct(ParserInterface $parser)
    {
        parent::__construct();

        $this->parser = $parser;
    }

    protected $name = 'generate';

    protected $description = 'Generate query deployment';

    protected function getOptions(): array
    {
        return [
            new InputOption('env', 'e', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'Which environments should be generated', 'prod'),
            new InputOption('from', 's', InputOption::VALUE_REQUIRED, 'Starting date will be included'),
            new InputOption('all', 'A', InputOption::VALUE_NONE, 'Should deployed SQL should be included', false),
        ];
    }

    public function handle()
    {
        //
    }
}
