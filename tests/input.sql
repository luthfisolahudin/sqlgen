# [meta.deploy]
# test:
# prod:

#
# This is root comment.
#
#
# Theres should be single blank space before this root comment.
# Below should be plain block in root table.
# Query defined in root table will always be executed before everything else.
#

# [pre]>

set @reused_var = true;
set @also_reused_var = true;

# This is comment on `up.when` table.
#
# This is also comment on `up.when` table with single space before this.
#
# Might as well I define what the purpose of `up.when` table.
# `up.when` table is used for define condition when query should only be execute,
# in other words query should only be executed when `up.when` is truthy.
#
# [up.when]>

select count(*) <= 0
from imaginary_table
where
    something = @reused_var and
    also_something = @also_reused_var;

# Above should be plain block in pre-condition table.
#
# `up` table is  where 'upgrade' query defined.
# This used for upgrade schema.
#
# [up.query]>

update imaginary_table
set nothing = @reused_var
where also_something = @also_reused_var;

# [down.when]>

# [down.query]>



# [finally]>

update imaginary_table
set updated_at = now();

#
# Query that defined in finally table will always be executed at the end
# whether main query is executed or not.
# Also, this comment should be appended to root table with single blank space before this block comment.
#
