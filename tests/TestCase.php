<?php

namespace LuthfiSolahudin\Sqlgen\Test;

use LaravelZero\Framework\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
